<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template • Todo</title>
		<link rel="stylesheet" href="{{asset('gama/css/base.css')}}">
		<link rel="stylesheet" href="{{asset('gama/css/index.css')}}">
		<!-- CSS overrides - remove if you don't need it -->
		<link rel="stylesheet" href="{{asset('gama/css/app.css')}}">
	</head>
	<body>
		<div id="app">
			<section class="todoapp">
				<header-component></header-component>
				<!-- This section should be hidden by default and shown when there are todos -->
				<main-component></main-component>
				<!-- This footer should be hidden by default and shown when there are todos -->
				{{-- <footer-component></footer-component> --}}
			</section>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
		<!-- Scripts here. Don't remove ↓ -->
		<script src="{{asset('gama/js/app.js')}}"></script>
	</body>
</html>
