import { createWebHistory, createRouter } from "vue-router";

import AllData from "../components/pages/AllData.vue";
import ActiveData from "../components/pages/ActiveData.vue";
import CompletedData from "../components/pages/CompletedData.vue";

const routes = [
    {
        name: "all",
        path: "/",
        component: AllData,
    },
    {
        name: "active",
        path: "/active",
        component: ActiveData,
    },
    {
        name: "completed",
        path: "/completed",
        component: CompletedData,
    },
    {
        path: "/:catchAll(.*)",
        component: () => import("../components/pages/NotFound.vue").default,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
