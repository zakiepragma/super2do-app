import { createApp } from "vue";
require("./bootstrap");
import router from "./router";

let app = createApp({});
app.component(
    "header-component",
    require("./components/HeaderComponent.vue").default
);
app.component(
    "main-component",
    require("./components/MainComponent.vue").default
);

app.use(router).mount("#app");
